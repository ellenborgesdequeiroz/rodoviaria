package model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import connection.ConnectionFactory;
import model_bean.Passageiro;

public class PassageiroDAO {
	
	public void create(Passageiro p) {
		Connection con = (Connection) ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = (PreparedStatement) con.prepareStatement("INSERT INTO passageiro (nome, genero, rg, cpf, endereco, email, telefone) VALUES (?,?,?,?,?,?,?)");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setString(3, p.getRg());
			stmt.setString(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setString(7, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Passageiro> read(){
		Connection con = (Connection) ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro p = new Passageiro();
				p.setId(rs.getInt("id_passageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getString("rg"));
				p.setCpf(rs.getString("cpf"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getString("telefone"));
				passageiros.add(p);
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}

public void delete(Passageiro p) {
		
		Connection con = (Connection) ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = (PreparedStatement) con.prepareStatement("DELETE FROM passageiro WHERE id_passageiro=?");
			stmt.setInt(1, (int) p.getId());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passageiro exclu�do com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: "+ e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}

public Passageiro read(int id) {
	Connection con = (Connection) ConnectionFactory.getConnection();
	PreparedStatement stmt = null;
	ResultSet rs = null;
	Passageiro p = new Passageiro();
	
	try {
		stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM passageiro WHERE id_passageiro=? LIMIT 1;");
		stmt.setInt(1, id);
		rs = stmt.executeQuery();
		if(rs != null && rs.next()) {
			p.setId(rs.getInt("id_passageiro"));
			p.setNome(rs.getString("nome"));
			p.setGenero(rs.getString("genero"));
			p.setRg(rs.getString("rg"));
			p.setCpf(rs.getString("cpf"));
			p.setEndereco(rs.getString("endereco"));
			p.setEmail(rs.getString("email"));
			p.setTelefone(rs.getString("telefone"));
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		ConnectionFactory.closeConnection(con, stmt, rs);
	}
	return p;
}

public void update(Passageiro p) {
	Connection con = (Connection) ConnectionFactory.getConnection();
	PreparedStatement stmt = null;
	
	try {
		stmt = (PreparedStatement) con.prepareStatement("UPDATE passageiro SET nome = ?, genero = ?, rg = ?, cpf = ?, endereco = ?, email = ?, telefone=? WHERE id_passageiro=?;");
		stmt.setString(1, p.getNome());
		stmt.setString(2, p.getGenero());
		stmt.setString(3, p.getRg());
		stmt.setString(4, p.getCpf());
		stmt.setString(5, p.getEndereco());
		stmt.setString(6, p.getEmail());
		stmt.setString(7, p.getTelefone());
		stmt.setInt(8, p.getId());
		stmt.executeUpdate();
		
		JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
	}catch (SQLException e) {
		JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
	} finally {
		ConnectionFactory.closeConnection(con, stmt);
	}
}
	
}
