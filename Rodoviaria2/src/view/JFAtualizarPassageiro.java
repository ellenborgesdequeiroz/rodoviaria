package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.dao.PassageiroDAO;
import model_bean.Passageiro;

import javax.swing.JLabel;
import java.awt.Color;

public class JFAtualizarPassageiro extends JFrame {
	private JPanel contentPane;
	
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtEndereco;
	private JTextField txtTelefone;
	private JTextField txtRg;
	private JTextField txtCpf;
	private JTextField txtGenero;
	private static int id; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 418, 366);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pDAO = new PassageiroDAO();
		Passageiro p = pDAO.read(id);

		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setBounds(26, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		final JLabel lblid = new JLabel("New label");
		lblid.setBounds(47, 11, 46, 14);
		contentPane.add(lblid);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setBounds(26, 24, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		txtNome = new JTextField();
		txtNome.setBounds(26, 37, 352, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Email");
		lblNewLabel_2.setBounds(26, 66, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(26, 80, 352, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("CPF");
		lblNewLabel_3.setBounds(215, 204, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setBounds(26, 204, 58, 14);
		contentPane.add(lblRg);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(26, 173, 352, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		

		txtGenero = new JTextField();
		txtGenero.setBounds(26, 127, 131, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(167, 127, 211, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("G\u00EAnero");
		lblNewLabel_4.setBounds(26, 111, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(167, 111, 94, 14);
		contentPane.add(lblTelefone);
		
		JLabel lblNewLabel_5 = new JLabel("Endere\u00E7o");
		lblNewLabel_5.setBounds(26, 158, 97, 14);
		contentPane.add(lblNewLabel_5);
		
		txtRg = new JTextField();
		txtRg.setBounds(26, 219, 168, 20);
		contentPane.add(txtRg);
		txtRg.setColumns(10);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(215, 219, 163, 20);
		contentPane.add(txtCpf);
		txtCpf.setColumns(10);
		
		lblid.setText(String.valueOf(p.getId()));
		txtNome.setText(p.getNome());
		txtEmail.setText(p.getEmail());
		txtGenero.setText(p.getGenero());
		txtEndereco.setText(p.getEndereco());
		txtTelefone.setText(p.getTelefone());
		txtRg.setText(p.getRg());
		txtCpf.setText(p.getCpf());
		
		
		JButton botaoCadastro = new JButton("Concluir Altera��o");
		botaoCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setId(Integer.parseInt(lblid.getText()));
				
				p.setNome(txtNome.getText());
				p.setEmail(txtEmail.getText());
				p.setGenero(txtGenero.getText());
				p.setEndereco(txtEndereco.getText());
				p.setTelefone(txtTelefone.getText());
				p.setRg(txtRg.getText());
				p.setCpf(txtCpf.getText());
				
				
				dao.update(p);
				dispose();
			}
		});
		botaoCadastro.setBounds(204, 263, 174, 23);
		contentPane.add(botaoCadastro);
		
		
	}
}
