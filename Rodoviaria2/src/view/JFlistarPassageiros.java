package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.dao.PassageiroDAO;
import model_bean.Passageiro;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JFlistarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTpassageiro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFlistarPassageiros frame = new JFlistarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFlistarPassageiros() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 560, 376);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Lista de Passageiros");
		lblNewLabel.setBounds(20, 22, 221, 14);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 47, 510, 150);
		contentPane.add(scrollPane);
		
		JTpassageiro = new JTable();
		JTpassageiro.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id Passageiro", "Nome", "G\u00EAnero", "RG", "CPF", "Endere\u00E7o", "Email", "Telefone"
			}
		));
		scrollPane.setViewportView(JTpassageiro);
		
		JButton btnCadastrar = new JButton("Cadastrar Passageiro");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFCadastroPassageiro cp = new JFCadastroPassageiro();
				cp.setVisible(true);				
			}
		});
		btnCadastrar.setBounds(10, 291, 177, 23);
		contentPane.add(btnCadastrar);
			
		
		JButton btnAlterar = new JButton("Alterar Passageiro");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JTpassageiro.getSelectedRow()!= -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTpassageiro.getValueAt(JTpassageiro.getSelectedRow(),0));
					ap.setVisible(true);	
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um Passageiro!pfv :) ");
				}
			}
		});
		btnAlterar.setBounds(227, 291, 143, 23);
		contentPane.add(btnAlterar);
		
		JButton btnExcluir = new JButton("Excluir Passageiro");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JTpassageiro.getSelectedRow()!= -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?","Exclus�o", JOptionPane.YES_NO_OPTION);
					if(opcao == 0) {
						PassageiroDAO dao = new PassageiroDAO();
						Passageiro p = new Passageiro();
						p.setId((int)JTpassageiro.getValueAt(JTpassageiro.getSelectedRow(),0));
						dao.delete(p);
					}
				}else {
					JOptionPane.showMessageDialog(null,"Selecione um passageiro!");
				}
				readJTable();
			}
		});
		btnExcluir.setBounds(390, 291, 129, 23);
		contentPane.add(btnExcluir);
		
		readJTable();
	}
	
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTpassageiro.getModel();
		modelo.setNumRows(0);
		PassageiroDAO pdao = new PassageiroDAO();
		for(Passageiro p : pdao.read()) {
			modelo.addRow(new Object[] {
					p.getId(),
					p.getNome(),
					p.getGenero(),
					p.getRg(),
					p.getCpf(),
					p.getEndereco(),
					p.getEmail(),
					p.getTelefone()
			});
		}
	}
}
