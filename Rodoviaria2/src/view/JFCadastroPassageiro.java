package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.dao.PassageiroDAO;
import model_bean.Passageiro;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;

public class JFCadastroPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtEndereco;
	private JTextField txtTelefone;
	private JTextField txtRg;
	private JTextField txtCpf;
	private JTextField txtGenero;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastroPassageiro frame = new JFCadastroPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastroPassageiro() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 417, 346);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 245, 238));
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cadastro de Passageiro");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(121, 0, 150, 26);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setBounds(26, 24, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		txtNome = new JTextField();
		txtNome.setBounds(26, 37, 352, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Email");
		lblNewLabel_2.setBounds(26, 66, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(26, 80, 352, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("CPF");
		lblNewLabel_3.setBounds(215, 204, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setBounds(26, 204, 58, 14);
		contentPane.add(lblRg);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(26, 173, 352, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		

		txtGenero = new JTextField();
		txtGenero.setBounds(26, 127, 131, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(167, 127, 211, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("G\u00EAnero");
		lblNewLabel_4.setBounds(26, 111, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(167, 111, 94, 14);
		contentPane.add(lblTelefone);
		
		JLabel lblNewLabel_5 = new JLabel("Endere\u00E7o");
		lblNewLabel_5.setBounds(26, 158, 97, 14);
		contentPane.add(lblNewLabel_5);
		
		txtRg = new JTextField();
		txtRg.setBounds(26, 219, 168, 20);
		contentPane.add(txtRg);
		txtRg.setColumns(10);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(215, 219, 163, 20);
		contentPane.add(txtCpf);
		txtCpf.setColumns(10);
		
		JButton botaoCadastro = new JButton("Concluir Cadastro");
		botaoCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setNome(txtNome.getText());
				p.setEmail(txtEmail.getText());
				p.setGenero(txtGenero.getText());
				p.setEndereco(txtEndereco.getText());
				p.setTelefone(txtTelefone.getText());
				p.setRg(txtRg.getText());
				p.setCpf(txtCpf.getText());
				
				
				dao.create(p);
				dispose();
			}
		});
		botaoCadastro.setBounds(247, 263, 131, 23);
		contentPane.add(botaoCadastro);
		
	}
}
