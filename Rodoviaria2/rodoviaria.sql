CREATE DATABASE rodoviaria;
USE rodoviaria;
CREATE TABLE `passageiro` (
  `id_passageiro` int(255) PRIMARY KEY AUTO_INCREMENT NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `rg` varchar(12) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(13) DEFAULT NULL
); 